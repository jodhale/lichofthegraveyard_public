using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityAtoms.BaseAtoms;
using Sirenix.OdinInspector;

public class Unit : Character
{
    [field: SerializeField, BoxGroup("Unit Information")] public UnitFaction unitFaction { get; private set; }
    [field: SerializeField, BoxGroup("Unit Information")] public UnitType unitType { get; private set; }
    [field: SerializeField, BoxGroup("Unit Properties")] public FloatConstant baseHealthPoints { get; private set; }
    [field: SerializeField, BoxGroup("Unit Properties")] public FloatConstant baseSpeed { get; private set; }
    [field: SerializeField, BoxGroup("Unit Properties")] public FloatConstant baseAttackDamage { get; private set; }
}
