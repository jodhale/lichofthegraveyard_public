using UnityEngine;
using Sirenix.OdinInspector;

public abstract class Character : MonoBehaviour
{
    [field: SerializeField, BoxGroup("Character Settings")] public string characterName { get; private set; }
}
