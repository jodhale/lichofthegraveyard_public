using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.UI;
using UnityAtoms.BaseAtoms;

public class SliderDisplay : MonoBehaviour
{
    [SerializeField] Slider sliderObject;

    public void SetSliderValue(FloatVariable floatVariable)
    {
        sliderObject.value = floatVariable.Value;
    }

    public void SetMaxValue(float maxValue)
    {
        sliderObject.maxValue = maxValue;
    }

    public void SetSliderValue(float value)
    {
        sliderObject.value = value;
    }
}
