using UnityEngine;
using UnityEngine.UI;
using UnityAtoms.BaseAtoms;

public class ImageFillDisplay : MonoBehaviour
{
    [SerializeField] private float maxValue;
    [SerializeField] private Image image;

    public void SetFillValue(FloatVariable value)
    {
        image.fillAmount = Calculation.NormalizedValue(value.Value, 0, maxValue);
    }
}
