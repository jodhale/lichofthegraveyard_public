using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasHandlerButton : ButtonBehavior
{
    [SerializeField] private Canvas menuMainCanvas;

    public override void ButtonFunction()
    {
        menuMainCanvas.enabled = !menuMainCanvas.enabled;
    }
}
