using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityAtoms.BaseAtoms;
using UnityEngine.Events;

public class IntegerTextEvent : MonoBehaviour
{
	[SerializeField] TextMeshProUGUI textGUI;
	[SerializeField] string prefix;
	[SerializeField] string suffix;
	[SerializeField] UnityEvent OnTextChange;

	public void ChangeIntText(int val)
	{
		textGUI.text = prefix + val.ToString() + suffix;
		OnTextChange.Invoke();
	}
}
