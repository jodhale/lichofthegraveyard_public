using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonSwitchBehavior : ButtonBehavior
{
    [SerializeField] private GameObject itemToSwitch;
    private bool isOn;

    public bool IsOn { get => isOn; set => isOn = value; }

    public override void ButtonFunction()
    {
        itemToSwitch.gameObject.SetActive(!itemToSwitch.activeSelf);
    }
}
