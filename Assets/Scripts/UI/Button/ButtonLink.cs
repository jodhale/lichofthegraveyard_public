using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

#pragma warning disable 0649

public class ButtonLink : ButtonBehavior
{
    [SerializeField] private string websiteLink;

    public override void ButtonFunction()
    {
        Application.OpenURL(websiteLink);
    }
}
