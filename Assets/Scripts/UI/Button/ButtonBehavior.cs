using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]

public abstract class ButtonBehavior : MonoBehaviour
{
    [SerializeField] private Button buttonObject;

    public Button ButtonObject { get => buttonObject; }

    private void Awake()
    {
        buttonObject.onClick.AddListener(ButtonFunction);
    }

    public abstract void ButtonFunction();
}
