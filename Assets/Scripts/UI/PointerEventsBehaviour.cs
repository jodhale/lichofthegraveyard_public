using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using UnityEngine.Events;
using UnityEngine.EventSystems;

public class PointerEventsBehaviour : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler
{
	[SerializeField] UnityEvent _OnPointerEnter;
	[SerializeField] UnityEvent _OnPointerExit;
	[SerializeField] UnityEvent _OnPointerDown;
	[SerializeField] UnityEvent _OnPointerUp;

	bool hasEntered;

	public void OnPointerDown(PointerEventData eventData)
	{
		_OnPointerDown.Invoke();
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		hasEntered = true;
		_OnPointerEnter.Invoke();
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		hasEntered = false;
		_OnPointerExit.Invoke();
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		if (hasEntered)
		{
			_OnPointerUp.Invoke();
		}
	}
}
