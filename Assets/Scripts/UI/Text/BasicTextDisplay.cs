using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityAtoms.BaseAtoms;

public class BasicTextDisplay : MonoBehaviour
{
    [SerializeField] protected TextMeshProUGUI textDisplay;
    private bool isFading;
    private const float fillAdder = 0.01f;

    private void Reset()
    {
        textDisplay = GetComponent<TextMeshProUGUI>();
    }
    public virtual void UpdateText(string textValue)
    {
        textDisplay.text = textValue;
    }

    public virtual void UpdateText(FloatVariable floatVariable)
    {
        textDisplay.text = floatVariable.Value.ToString();
    }

    public virtual void UpdateText(StringVariable stringVariable)
    {
        textDisplay.text = stringVariable.Value;
    }

    public virtual void UpdateText(IntVariable intVariable)
    {
        textDisplay.text = intVariable.Value.ToString();
    }
}
