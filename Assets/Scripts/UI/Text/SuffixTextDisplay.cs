using UnityAtoms.BaseAtoms;

public class SuffixTextDisplay : BasicTextDisplay
{
    public string suffix;

    public override void UpdateText(IntVariable intVariable)
    {
        textDisplay.text = intVariable.Value.ToString() + suffix;
    }
}
