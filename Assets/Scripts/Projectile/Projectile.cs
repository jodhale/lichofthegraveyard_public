using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] private float projectileSpeed;
    [SerializeField] private Damager damager;

    private Transform targetObject;
    private bool isLaunching;

    public void Initialize(Transform targetToSet)
    {
        targetObject = targetToSet;
        isLaunching = true;
        damager.Initialize(targetToSet.GetComponent<Unit>());
    }

    private void Update()
    {
        if (!isLaunching) return;

        Vector3 direction = targetObject.position - transform.position;
        direction.Normalize();
        transform.position += direction * Time.deltaTime * projectileSpeed;
    }
}
