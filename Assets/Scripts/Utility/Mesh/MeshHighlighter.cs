using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class MeshHighlighter : MonoBehaviour
{
    [SerializeField] private Color highlightedColor;
    [SerializeField] private Color unusableColor;
    [SerializeField] private List<MeshRenderer> objectRenderers;
    private List<Color> originalColors;
    private void Awake()
    {
        originalColors = new List<Color>();

        foreach(MeshRenderer renderer in objectRenderers)
        {
            originalColors.Add(renderer.material.color);
        }
    }

    public void SetHighlighted(bool value)
    {
        if (value)
        {
            SetRendererColors(highlightedColor);
        }
        else
        {
            SetColorToDefault();
        }
    }

    public void SetColorUnusable(bool value)
    {
        if (value)
        {
            SetRendererColors(unusableColor);
        }
        else
        {
            SetColorToDefault();
        }
    }

    private void SetRendererColors(Color color)
    {
        for (int currentIndex = 0; currentIndex < objectRenderers.Count; currentIndex++)
        {
            objectRenderers[currentIndex].material.color = color;
        }
    }

    public void SetColorToDefault()
    {
        for (int currentIndex = 0; currentIndex < objectRenderers.Count; currentIndex++)
        {
            objectRenderers[currentIndex].material.color = originalColors[currentIndex];
        }
    }
}
