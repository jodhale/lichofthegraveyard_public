using UnityEngine;
using Sirenix.OdinInspector;

public abstract class ComponentPattern<T> : MonoBehaviour
{
    [BoxGroup("Entity Reference"), SerializeField] protected T entity;

    private void Awake()
    {
        if (entity is null)
            entity = GetComponent<T>();
        AwakeMethod();
    }

    public virtual void AwakeMethod() { }
}
