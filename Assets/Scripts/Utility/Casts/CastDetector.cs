using UnityEngine;
using Sirenix.OdinInspector;

public abstract class CastDetector : MonoBehaviour
{
    [SerializeField, BoxGroup("Configuration")] private bool isDetectionActive;
    [SerializeField, BoxGroup("Configuration")] protected Transform castStartingPoint;
    [SerializeField, BoxGroup("Configuration")] protected Vector3 castDirection;
    [SerializeField, BoxGroup("Configuration")] protected LayerMask detectableObjects;
    [SerializeField, BoxGroup("Configuration")] protected float castDistance;

    [SerializeField, FoldoutGroup("Debug")] protected Color gizmoColor;

    private RaycastHit2D[] hitObjects;
    private bool isHitting;

    public bool IsDetecting { get => isHitting; set => isHitting = value; }
    public RaycastHit2D[] HitObjects { get => hitObjects; set => hitObjects = value; }
    public bool IsDetectionActive { get => isDetectionActive; set => isDetectionActive = value; }

    private void Reset()
    {
        isDetectionActive = true;
        gizmoColor = Color.yellow;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isDetectionActive) return;

        CastAction();
    }

    protected abstract void CastAction();
}


