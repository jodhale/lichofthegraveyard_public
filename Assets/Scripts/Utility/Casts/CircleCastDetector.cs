using UnityEngine;
using Sirenix.OdinInspector;

public class CircleCastDetector : CastDetector
{
    [SerializeField, BoxGroup("Configuration")] private float circleRadius;

    protected override void CastAction()
    {
        RaycastHit2D currentHitInfo;
        HitObjects = new RaycastHit2D[1];

        currentHitInfo = Physics2D.CircleCast(castStartingPoint.position, circleRadius, castDirection, castDistance, detectableObjects);

        if (currentHitInfo.collider != null)
        {
            HitObjects[0] = currentHitInfo;
            IsDetecting = true;
        }
        else
        {
            IsDetecting = false;
        }
    }

    private void OnDrawGizmos()
    {
        if (castStartingPoint is null) return;

        Vector3 newPosition = castStartingPoint.position + (castDirection * castDistance);
        Gizmos.color = gizmoColor;
        Gizmos.DrawLine(castStartingPoint.position, newPosition);
        Gizmos.DrawWireSphere(newPosition, circleRadius);
    }
}
