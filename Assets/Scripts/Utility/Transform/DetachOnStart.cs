using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetachOnStart : MonoBehaviour
{
    private void Start()
    {
        transform.SetParent(null);
    }
}
