using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreserveRotationLookAt : MonoBehaviour
{
    [SerializeField] Vector3 axisPreset;

    private Vector3 standardTargetRotation;

    private void Start()
    {
        standardTargetRotation = axisPreset + Vector3.forward;
    }

    private void Update()
    {
        Quaternion newRotation = Quaternion.Euler(standardTargetRotation);
        transform.rotation = newRotation;
    }
}
