using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParentToAnotherTransform : MonoBehaviour
{
    [SerializeField] Transform targetTransform;

    // Start is called before the first frame update
    void Start()
    {
        transform.SetParent(targetTransform, true);
    }
}
