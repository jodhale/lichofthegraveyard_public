using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CustomExtension
{
    public static class ExtensionMethods 
    {
        public static T  GetRandom<T>(this List<T> list)
        {
            return list[Random.Range(0, list.Count)];
        }

        public static bool AnyAxisIsZero(this Rigidbody2D rigidBody)
        {
            if ((rigidBody.velocity.x != 0) && (rigidBody.velocity.y != 0)) return true;

            return false;
        }
    }
}