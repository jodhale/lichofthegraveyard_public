using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

public static class Helper 
{
    private static readonly Dictionary<float, WaitForSeconds> waitDictionary = new Dictionary<float, WaitForSeconds>();

    public static WaitForSeconds GetWait(float time)
    {
        if (waitDictionary.TryGetValue(time, out var wait)) return wait;

        waitDictionary[time] = new WaitForSeconds(time);
        return waitDictionary[time];
    }

    private static PointerEventData eventDataCurrentPosition;
    private static List<RaycastResult> results;

    public static bool isOverUI()
    {
        eventDataCurrentPosition = new PointerEventData(EventSystem.current) { position = Mouse.current.position.ReadValue()};
        results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }

    public static Vector2 GetWorldPositionOfCanvasElement(RectTransform element, Camera camera)
    {
        RectTransformUtility.ScreenPointToWorldPointInRectangle(element, element.position, camera, out var result);
        return result;
    }

    public static void DeleteChildren(this Transform targetTransform)
    {
        foreach (Transform child in targetTransform) Object.Destroy(child.gameObject);
    }

    public static void DeleteChildrenImmediate(this Transform targetTransform)
    {
        GameObject destroyObject = new GameObject();
        foreach (Transform child in targetTransform) child.transform.SetParent(destroyObject.transform);
        Object.DestroyImmediate(destroyObject);
    }

    public static bool GetRandomTrueOrFalse()
    {
        return Random.value > 0.5f;
    }

    public static bool IsClose(this Vector3 pointA, Vector3 pointB)
    {
        return (pointA - pointB).sqrMagnitude < ((pointA - pointB).magnitude);
    }

    public static Vector2 GetVector2(this Vector3 value)
    {
        return new Vector2(value.x, value.y);
    }
}
