using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnStartTrigger : MonoBehaviour
{
    [SerializeField] private UnityEvent onStartEvent;

    void Start()
    {
        onStartEvent.Invoke();
    }
}
