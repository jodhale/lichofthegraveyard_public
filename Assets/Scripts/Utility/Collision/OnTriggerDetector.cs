﻿using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

public class OnTriggerDetector : MonoBehaviour
{
    [BoxGroup("Events")] public UnityEvent onTriggerEnterLocalEvent;
    [BoxGroup("Events")] public UnityEvent onTriggerStayLocalEvent;
    [BoxGroup("Events")] public UnityEvent onTriggerExitLocalEvent;

    public event TriggerMethod onTriggerEnterEvent;
    public event TriggerMethod onTriggerStayEvent;
    public event TriggerMethod onTriggerExitEvent;

    public delegate void TriggerMethod(GameObject other);

    public void Register(ITriggerDetectable triggerDetectable)
    {
        onTriggerEnterEvent += triggerDetectable.OnTriggerEnterOverride;
        onTriggerStayEvent += triggerDetectable.OnTriggerStayOverride;
        onTriggerExitEvent += triggerDetectable.OnTriggerExitOverride;
    }

    public void UnRegister(ITriggerDetectable triggerDetectable)
    {
        onTriggerEnterEvent -= triggerDetectable.OnTriggerEnterOverride;
        onTriggerStayEvent -= triggerDetectable.OnTriggerStayOverride;
        onTriggerExitEvent -= triggerDetectable.OnTriggerExitOverride;
    }

    private void OnTriggerEnter(Collider other)
    {
        onTriggerEnterLocalEvent.Invoke();

        if (onTriggerEnterEvent == null) return;

        onTriggerEnterEvent(other.gameObject);
    }

    private void OnTriggerStay(Collider other)
    {
        onTriggerStayLocalEvent.Invoke();

        if (onTriggerStayEvent == null) return;
        onTriggerStayEvent(other.gameObject);
    }

    private void OnTriggerExit(Collider other)
    {
        onTriggerExitLocalEvent.Invoke();

        if (onTriggerExitEvent == null) return;

        onTriggerExitEvent(other.gameObject);
    }
}

public interface ITriggerDetectable
{
    void OnTriggerEnterOverride(GameObject other);
    void OnTriggerStayOverride(GameObject other);
    void OnTriggerExitOverride(GameObject other);
}