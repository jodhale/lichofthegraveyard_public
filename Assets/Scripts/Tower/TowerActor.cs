using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityAtoms.BaseAtoms;
using Sirenix.OdinInspector;

public class TowerActor : MonoBehaviour, IDamageable
{
    [SerializeField] private SOTowerActor towerActorReference;

    [SerializeField] private FloatVariable curHealth;
    [SerializeField] private VoidEvent OnPlayerLose;
    [SerializeField] private Image hpBar;

    private void Awake()
    {
        towerActorReference.Value = this;
	}

    void Start()
    {
        curHealth.Reset();
    }

    [Button]
    public void Damage(int value)
    {
        curHealth.Value -= value;
        hpBar.fillAmount = curHealth.Value / curHealth.InitialValue;

        if (curHealth.Value <= 0)
        {
			OnPlayerLose.Raise();
        }
    }
}
