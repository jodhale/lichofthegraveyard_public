using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Variable/Tower Actor")]
public class SOTowerActor : SOVariable<TowerActor> { }
