using UnityEngine;
using Sirenix.OdinInspector;
using System.Collections.Generic;
using System.Collections;
using System.Linq;

public class Interactable : MonoBehaviour
{
    [SerializeField] private bool isInteracting = true;
    private List<IInteractable> interactables;

    private void Awake()
    {
        interactables = GetComponents<IInteractable>().ToList();
    }

    public void Interact(Interactor interactor)
    {
        if (!isInteracting) return;

        foreach (IInteractable currentInteractable in interactables)
        {
            currentInteractable.React(interactor);
        }
    }
}

public interface IInteractable
{
    void React(Interactor interactor);
}