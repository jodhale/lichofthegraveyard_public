using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.InputSystem;
using UnityEngine.Events;
using UnityAtoms.BaseAtoms;

public class Interactor : MonoBehaviour
{

    [SerializeField] LayerMask interactableLayers;
    [SerializeField] private SOCamera mainCamera;
    private Ray ray;
    private RaycastHit hitObject;

    public InteractableEvent onInteractWithInteractable;

    public void LeftClickAndInteract()
    {
        if (Helper.isOverUI()) return;

        ray = mainCamera.Value.ScreenPointToRay(Mouse.current.position.ReadValue());
        if (Physics.Raycast(ray, out hitObject, Mathf.Infinity, interactableLayers))
        {
            if (hitObject.collider.gameObject.TryGetComponent<Interactable>(out Interactable interactableComponent))
            {
                interactableComponent.Interact(this);
                onInteractWithInteractable.Invoke(interactableComponent);
            }
        }
    }

    public void RightClickAndInteract()
    {
        if (Helper.isOverUI()) return;


    }
}

public interface ILeftClickInteractor
{
    void LeftClickInteract(Interactable interactable);
}

public interface IRightClickInteractor
{
    void RightClickInteract(Interactable interactable);
}

[System.Serializable]
public class InteractableEvent : UnityEvent<Interactable> { }