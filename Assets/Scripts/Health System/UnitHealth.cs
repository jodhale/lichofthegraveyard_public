using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using PixelDust.Audiophile;

public class UnitHealth : MonoBehaviour, IDamageable
{
    [SerializeField] private int maxHealth;

    [SerializeField] private UnityEvent<float> OnMaxHealthSet;
    [SerializeField] private UnityEvent<float> OnHealthChange;
    [SerializeField] private UnityEvent OnCharacterDie;

    [SerializeField] private SoundEvent hitSFX, deathSFX;


    private int currentHealth;

    public float CurrentHealth
    {
        get
        {
            return currentHealth;
        }
    }

    public bool IsDead
    {
        get
        {
            return currentHealth <= 0;
        }
    }

    private void Start()
    {
        currentHealth = maxHealth;
        OnMaxHealthSet.Invoke(maxHealth);
        OnHealthChange.Invoke(currentHealth);
    }

    public void AddHealth(int healthToAdd)
    {
        if (IsDead) return;

        int newHealth = currentHealth + healthToAdd;

        currentHealth = ClampHealthValue(newHealth);

        OnHealthChange.Invoke(currentHealth);
    }

    public void SubtractHealth(int healthToSubtract)
    {
        if (IsDead) return;

        int newHealth = currentHealth - healthToSubtract;

        currentHealth = ClampHealthValue(newHealth);

        OnHealthChange.Invoke(currentHealth);

        if(currentHealth <= 0)
        {
            OnCharacterDie.Invoke();
        }
    }
    
    private int ClampHealthValue(int newValue)
    {
        return Mathf.Clamp(newValue,0, maxHealth);
    }

    public void Damage(int value)
    {
        SubtractHealth(value);
        hitSFX.Play();
    }

    [Button]
    public void TriggerDeathEvent()
    {
        deathSFX.Play();
        OnCharacterDie.Invoke();
    }
}
