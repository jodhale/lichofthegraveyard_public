using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.AI;

public class WaypointSequenceHandler : MonoBehaviour
{
    [SerializeField] private Color debugColor;
    [SerializeField] private List<Transform> waypointTarget;

    public void AssignWaypoint(UnitWaypointBehavior waypointBehavior)
    {
        waypointBehavior.Initialize(this);
        waypointBehavior.SetStartingPosition(waypointTarget[0].position);
        waypointBehavior.SetStartingWaypoint(waypointTarget[1]);
    }

    public int TotalWaypoints
    {
        get
        {
            return waypointTarget.Count;
        }
    }


    public Transform GetNextWaypoint(int waypointIndex)
    {
        return (waypointIndex >= waypointTarget.Count) ? null : waypointTarget[waypointIndex];
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        if (waypointTarget.Count >= 2)
        {
            Gizmos.color = debugColor;
            for (int pointIndex = 0; pointIndex < waypointTarget.Count; pointIndex++)
            {
                if (pointIndex < waypointTarget.Count - 1)
                    Gizmos.DrawLine(waypointTarget[pointIndex].position, waypointTarget[pointIndex + 1].position);
            }
        }
    }
#endif
}
