using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CustomExtension;

public class WaypointManager : Singleton<WaypointManager>
{
    [SerializeField] private List<WaypointSequenceHandler> waypointSequence;

    public void SetWaypointSequenceToUnit(Unit unitToSet)
    {
        if (unitToSet.TryGetComponent(out UnitWaypointBehavior waypointBehavior))
        {
            waypointSequence.GetRandom().AssignWaypoint(waypointBehavior);
        }
    }
}
