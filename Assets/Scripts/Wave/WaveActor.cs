using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityAtoms.BaseAtoms;
using TMPro;
using UnityEngine.UI;

public class WaveActor : MonoBehaviour
{
    [SerializeField] List<Transform> spawnPoints;
    [SerializeField] List<UnitType> unitTypes;
    [SerializeField] FloatConstant spawnInterval;
    [SerializeField] IntConstant addedSpawn;
    [SerializeField] IntConstant startingSpawn;
    [SerializeField] IntConstant waveIntervalIncrease;

    [SerializeField] TextMeshProUGUI waveText;
    [SerializeField] Image fillUI;

    float nextSpawnDuration;
    int curSpawnCount;
    int curWave;

    void Start()
	{
		curSpawnCount = startingSpawn.Value;
		SpawnWave();
	}

    void Update()
    {
        nextSpawnDuration += Time.deltaTime;
        fillUI.fillAmount = nextSpawnDuration / spawnInterval.Value;

        if (nextSpawnDuration > spawnInterval.Value)
        {
            SpawnWave();
        }
    }

    public void SpawnWave()
    {
        List<UnitType> unitTypes = new List<UnitType>(this.unitTypes);

        nextSpawnDuration = 0;
        curWave++;
        waveText.text = "WAVE " + curWave;

        if (curWave % waveIntervalIncrease.Value == 0)
        {
            curSpawnCount += curSpawnCount + addedSpawn.Value;
        }

        int tempCurSpawnCount = curSpawnCount;

        for (int i = 0; i < curSpawnCount; i++)
        {
            //cost check
            unitTypes.RemoveAll(x => x.SoulCost > tempCurSpawnCount);
            UnitType unit = unitTypes[Random.Range(0, unitTypes.Count)];
            tempCurSpawnCount -= (int)unit.SoulCost;
            //actual unit spawning
            Unit spawnedUnit = UnitFactory.Instance.CreateUnit(unit.AdventurerUnitReference.gameObject);
            WaypointManager.Instance.SetWaypointSequenceToUnit(spawnedUnit);

            if (tempCurSpawnCount == 0)
            {
                break;
            }
        }
    }
}
