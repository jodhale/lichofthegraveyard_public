using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System.Linq;

[CreateAssetMenu(menuName = "Unit Management/Data")]
public class SelectionData : SerializedScriptableObject
{
    [SerializeField, ReadOnly] private Dictionary<int, Unit> selectedTable;

    [SerializeField] private UnitFaction factionSelectable;

    public Dictionary<int, Unit> SelectedTable { get => selectedTable; }

    private List<Unit> unitsSelected;

    public void InitializeTable()
    {
        selectedTable = new Dictionary<int, Unit>();
        unitsSelected = new List<Unit>();
    }

    public void SelectUnit(Unit unitToSelect)
    {
        if (unitToSelect.unitFaction != factionSelectable) return;

        int objectID = unitToSelect.GetInstanceID();

        if (!selectedTable.ContainsKey(objectID))
        {
            selectedTable.Add(objectID, unitToSelect);

            if (unitToSelect.TryGetComponent<UnitSelection>(out UnitSelection selectionComponent))
            {
                selectionComponent.SelectUnit();
            }
        }
    }

    public void DeselectUnit(Unit unitToDeselect)
    {

        if (selectedTable.TryGetValue(unitToDeselect.GetInstanceID(), out Unit unitObject))
        {
            if (unitToDeselect.TryGetComponent<UnitSelection>(out UnitSelection selectionComponent))
            {
                selectionComponent.DeselectUnit();
            }

            selectedTable.Remove(unitToDeselect.GetInstanceID());
        }

    }

    public void DeselectAllUnits()
    {
        foreach (KeyValuePair<int, Unit> unitData in selectedTable)
        {
            if (unitData.Value != null)
            {
                if (selectedTable[unitData.Key].TryGetComponent<UnitSelection>(out UnitSelection selectionComponent))
                {
                    selectionComponent.DeselectUnit();
                }
            }
        }

        selectedTable.Clear();
    }

    public List<Unit> GetAllSelectedObjects()
    {
        unitsSelected.Clear();

        foreach (KeyValuePair<int, Unit> unitData in selectedTable)
        {
            unitsSelected.Add(unitData.Value);
        }

        return unitsSelected;
    }
}
