using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityAtoms.BaseAtoms;
using UnityEngine.UI;
using UnityEngine.Events;

public class UnitSelectorButton : MonoBehaviour
{
    [SerializeField] private UnitType unitToSelect;
    [SerializeField] private GameObject unitObject;
    [SerializeField] private GameObjectVariable unitToSpawn;
    [SerializeField] private IntVariable currentTotalSoul;
    [SerializeField] private Button buttonObject;

    [SerializeField] private UnityEvent OnSelect, OnDeselect;

    private void Awake()
    {
        buttonObject.onClick.AddListener(SelectThis);
    }

    private void SelectThis()
    {
        UnitManager.Instance.SelectRoster(this);
    }

    public void ProcessSelection()
    {
        if (currentTotalSoul.Value < unitToSelect.SoulCost) return;

        unitToSpawn.Value = unitObject;
        OnSelect.Invoke();
    }

    public bool HasEnoughSoul()
    {
        return (currentTotalSoul.Value >= unitToSelect.SoulCost);
    }

    public void Deselect()
    {
        OnDeselect.Invoke();
    }

    public void ProcessCost()
    {
        if (currentTotalSoul.Value < unitToSelect.SoulCost) return;

        currentTotalSoul.Value -= (int)unitToSelect.SoulCost;
    }
}
