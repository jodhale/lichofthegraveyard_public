using UnityEngine;
using UnityAtoms.BaseAtoms;
using TMPro;
using UnityEngine.UI;

public class UnitSelectionVisuals : MonoBehaviour
{
    [SerializeField] private TMP_Text totalCountText;
    [SerializeField] private TMP_Text costText;
    [SerializeField] private TMP_Text keyControlText;
	[SerializeField] Image unitBackground;
	[SerializeField] Sprite selectedSprite;
	[SerializeField] Sprite deselectedSprite;

    public string KeyType;
    public UnitType UnitToDisplay;

    private void Awake()
    {
        costText.text = UnitToDisplay.SoulCost.ToString();
        keyControlText.text = KeyType;
    }

    public void UpdateVisuals(int currentTotalUnitsUpdatedValue)
    {
        totalCountText.text = currentTotalUnitsUpdatedValue.ToString();
    }

	public void ToggleSelectionUI(bool isSelected) => unitBackground.sprite = isSelected ? selectedSprite : deselectedSprite;
}
