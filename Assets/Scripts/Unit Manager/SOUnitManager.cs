using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Variable/Unit Manager")]
public class SOUnitManager : SOVariable<UnitManager> { }
