using UnityEngine;
using Sirenix.OdinInspector;
using UnityAtoms.BaseAtoms;
using UnityEngine.InputSystem;
using UnityEngine.Events;
using PixelDust.Audiophile;
using System.Collections;
using System.Collections.Generic;

public class UnitManager : Singleton<UnitManager>
{
    //general variables

    [SerializeField, BoxGroup("General Settings")] private UnitManagerState currentState;

    //spawning variables

    [SerializeField, BoxGroup("Unit Spawning")] private GameObjectVariable unitToSpawn;
    [SerializeField, BoxGroup("Unit Spawning")] private SOUnitManager unitManager;
    [SerializeField, BoxGroup("Unit Spawning")] private SOCamera mainCamera;
    [SerializeField, BoxGroup("Unit Spawning")] private float spawnCheckRadius;
    [SerializeField, BoxGroup("Unit Spawning")] LayerMask spawnableLayers;
    [SerializeField] ParticleSystem pointerParticle;


    [SerializeField] private SoundEvent unitSummon;

    private Ray ray;
    private RaycastHit hitObject;

    private void Awake()
    {
        currentSelectorButton = null;
        selectionData.InitializeTable();
        currentState = UnitManagerState.Selecting;
        unitManager.Value = this;
        dragSelect = false;
    }

    private void OnGUI()
    {
        if (dragSelect == true)
        {
            var rect = SelectionUtility.GetScreenRect(startingSelectPosition, Mouse.current.position.ReadValue());
            SelectionUtility.DrawScreenRect(rect, new Color(0.8f, 0.8f, 0.95f, 0.25f));
            SelectionUtility.DrawScreenRectBorder(rect, 2, new Color(0.8f, 0.8f, 0.95f));
        }
    }

    #region SPAWNING UNITS

    public void SpawnOnPoint()
    {
        if (Helper.isOverUI() || (currentState != UnitManagerState.Spawning)) return;

        if (unitToSpawn.Value is null || currentSelectorButton is null) return;

        if (!currentSelectorButton.HasEnoughSoul()) return;

        ray = mainCamera.Value.ScreenPointToRay(Mouse.current.position.ReadValue());


        if (Physics.SphereCast(ray, spawnCheckRadius, out hitObject, Mathf.Infinity, spawnableLayers))
        {
            if (hitObject.collider.TryGetComponent<Unit>(out Unit unitCharacter)) return;

            Vector3 hitPosition = hitObject.point;
            hitPosition.y = 1;
            Unit newUnit = UnitFactory.Instance.CreateUnit(unitToSpawn.Value);
            newUnit.transform.position = hitPosition;
            currentSelectorButton.ProcessCost();
            unitSummon.Play();
        }
    }

    #endregion

    #region SELECTING UNITS
    //Selection Variables

    [SerializeField, BoxGroup("Unit Selection")] private float dragDistanceToRegister;
    [SerializeField, BoxGroup("Unit Selection")] private SelectionData selectionData;
    [SerializeField, BoxGroup("Unit Selection")] private LayerMask selectionLayers;
    [SerializeField, BoxGroup("Unit Selection")] private LayerMask selectionGroundLayer;

    private MeshCollider selectionBox;
    private Mesh selectionMesh;

    Vector2[] corners;

    Vector3[] vecs;
    Vector3[] verts;


    private Vector3 startingSelectPosition;
    private Vector3 endSelectPosition;
    private bool isInclusive;
    private bool dragSelect = true;

    private RaycastHit selectHitObject;
    private RaycastHit cancelHitObject;


    //when button down
    public void StartSelect()
    {
        if (currentState == UnitManagerState.Spawning) return;

        startingSelectPosition = Mouse.current.position.ReadValue();
    }

    //while pressing
    public void CheckAndSetIfDragSelecting()
    {
        if (currentState == UnitManagerState.Spawning) return;


        endSelectPosition = Mouse.current.position.ReadValue();

        if ((startingSelectPosition - endSelectPosition).magnitude > dragDistanceToRegister) dragSelect = true;
    }

    //when player presses shift
    public void SetSelectionInclusive(bool value)
    {
        isInclusive = value;
    }

    // on button release
    public void ApplySelection()
    {
        if (currentState == UnitManagerState.Spawning)
        {
            dragSelect = false;
            return;
        }

        if (!dragSelect)
        {
            Ray ray = mainCamera.Value.ScreenPointToRay(startingSelectPosition);

            if (Physics.Raycast(ray, out selectHitObject, Mathf.Infinity, selectionLayers))
            {
                if (isInclusive)
                {
                    SelectUnitFromRaycast(selectHitObject);
                }
                else
                {
                    selectionData.DeselectAllUnits();
                    SelectUnitFromRaycast(selectHitObject);
                }
            }
            else
            {
                if (!isInclusive) selectionData.DeselectAllUnits();

            }
        }
        else
        {
            verts = new Vector3[4];
            vecs = new Vector3[4];
            int i = 0;
            endSelectPosition = Mouse.current.position.ReadValue();
            corners = GetBoundingBox(startingSelectPosition.GetVector2(), endSelectPosition.GetVector2());

            foreach (Vector2 corner in corners)
            {
                Ray cancelRay = mainCamera.Value.ScreenPointToRay(corner);

                if (Physics.Raycast(cancelRay, out cancelHitObject, Mathf.Infinity, selectionGroundLayer))
                {
                    verts[i] = new Vector3(cancelHitObject.point.x, cancelHitObject.point.y, cancelHitObject.point.z);
                    vecs[i] = cancelRay.origin - cancelHitObject.point;
                    Debug.DrawLine(mainCamera.Value.ScreenToWorldPoint(corner), cancelHitObject.point, Color.red, 1.0f);
                }
                i++;
            }

            //generate the mesh
            selectionMesh = GenerateSelectionMesh(verts, vecs);

            selectionBox = gameObject.AddComponent<MeshCollider>();
            selectionBox.sharedMesh = selectionMesh;
            selectionBox.convex = true;
            selectionBox.isTrigger = true;

            if (!isInclusive) selectionData.DeselectAllUnits();

            Destroy(selectionBox, 0.02f);

        }

        dragSelect = false;
    }

    //can be triggered from anywhere
    public void Deselect()
    {
        selectionData.DeselectAllUnits();
    }

    private void SelectUnitFromRaycast(RaycastHit hitObject)
    {
        selectionData.SelectUnit(hitObject.collider.GetComponent<Unit>());
    }

    private Vector2[] GetBoundingBox(Vector2 p1, Vector2 p2)
    {
        var bottomLeft = Vector3.Min(p1, p2);
        var topRight = Vector3.Max(p1, p2);

        Vector2[] corners =
        {
            new Vector2(bottomLeft.x, topRight.y),
            new Vector2(topRight.x, topRight.y),
            new Vector2(bottomLeft.x, bottomLeft.y),
            new Vector2(topRight.x, bottomLeft.y)
        };
        return corners;
    }

    private Mesh GenerateSelectionMesh(Vector3[] corners, Vector3[] vecs)
    {
        Vector3[] newVerts = new Vector3[8];
        int[] tris = { 0, 1, 2, 2, 1, 3, 4, 6, 0, 0, 6, 2, 6, 7, 2, 2, 7, 3, 7, 5, 3, 3, 5, 1, 5, 0, 1, 1, 4, 0, 4, 5, 6, 6, 5, 7 };

        for (int i = 0; i < 4; i++)
        {
            newVerts[i] = corners[i];
        }

        for (int j = 4; j < 8; j++)
        {
            newVerts[j] = corners[j - 4] + vecs[j - 4];
        }

        Mesh selectionMesh = new Mesh();
        selectionMesh.vertices = newVerts;
        selectionMesh.triangles = tris;

        return selectionMesh;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out Unit unit)) selectionData.SelectUnit(unit);
    }

    //for using commands
    [SerializeField, BoxGroup("Command System")] public MoveCommand moveCommand;
    [SerializeField, BoxGroup("Command System")] LayerMask commandReactableLayers;
    private Ray commandRay;
    private RaycastHit commandHitObject;


    #endregion

    #region COMMAND UNITS

    public void CommandUnits()
    {
        if (currentState == UnitManagerState.Spawning) return;

        commandRay = mainCamera.Value.ScreenPointToRay(Mouse.current.position.ReadValue());

        if (Physics.Raycast(commandRay, out commandHitObject, Mathf.Infinity, commandReactableLayers))
        {
            //just move by the default
            moveCommand.SetTargetPositionValue(commandHitObject.point);
            moveCommand.Execute(selectionData.GetAllSelectedObjects());
            Vector3 worldPosition = commandHitObject.point;
            worldPosition.y = 1;
            pointerParticle.gameObject.transform.position = worldPosition;
            pointerParticle.Play();
        }
    }

    #endregion

    #region Roster Selection

    [SerializeField] private List<UnitSelectorButton> rosterButtons;
    private UnitSelectorButton currentSelectorButton;

    public void SelectRoster(UnitSelectorButton selectorButton)
    {
        if (currentState != UnitManagerState.Spawning) selectionData.DeselectAllUnits();

        if (currentSelectorButton == selectorButton)
        {
            ActivateSpawningState(false);
            currentSelectorButton.Deselect();
            currentSelectorButton = null;
            return;
        }

        ActivateSpawningState(true);
        currentSelectorButton = selectorButton;
        currentSelectorButton.ProcessSelection();

        foreach (UnitSelectorButton otherSelector in rosterButtons)
        {
            if (otherSelector != currentSelectorButton)
                otherSelector.Deselect();
        }
    }

    public void SelectRosterByIndex(int index)
    {
        SelectRoster(rosterButtons[index]);
    }

    public void ActivateSpawningState(bool value)
    {
        if (value)
            currentState = UnitManagerState.Spawning;
        else
            currentState = UnitManagerState.Selecting;
    }


    #endregion
}

public enum UnitManagerState
{
    Selecting,
    Spawning
}
