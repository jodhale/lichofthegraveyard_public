using UnityEngine;
using UnityEngine.Events;

public class CommandReactable : MonoBehaviour
{
    [field: SerializeField] public Command commandToReactTo { get; private set; }

    [SerializeField] UnityEvent OnReactToCommand;

    public void ReactToCommand()
    {
        OnReactToCommand?.Invoke();
    }
}
