using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Unit Management/Commands")]
public class MoveCommand : Command
{
    private Vector3 targetPosition;

    public override void Execute(List<Unit> units)
    {
        List<Vector3> targetPositionList = GetPositiongListAround(targetPosition, 3f, units.Count);

        int positionIndex = 0;

        foreach (Unit unitInCommand in units)
        {
            unitInCommand.GetComponent<UnitMovementBehavior>().MoveTowardsPoint(targetPositionList[positionIndex]);
            unitInCommand.GetComponent<UnitCommandHandler>().TriggerMove();
            positionIndex = (positionIndex + 1) % targetPositionList.Count;
        }
    }

    public override bool IsExecutable(CommandReactable commandReactable)
    {
        return (commandReactable == null);
    }

    public void SetTargetPositionValue(Vector3 targetPosition)
    {
        this.targetPosition = targetPosition;
    }

    private List<Vector3> GetPositiongListAround(Vector3 startingPosition, float distance, int positionCount)
    {
        List<Vector3> positionList = new List<Vector3>();

        for (int i = 0; i < positionCount; i++)
        {
            float angle = i * (360f / positionCount);
            Vector3 dir = ApplyRotationToVector(new Vector3(1,0), angle);
            Vector3 position = startingPosition + dir * distance;
            positionList.Add(position);
        }

        return positionList;
    }

    private Vector3 ApplyRotationToVector(Vector3 vec, float angle)
    {
        return Quaternion.Euler(0, 0, angle) * vec;
    }
}
