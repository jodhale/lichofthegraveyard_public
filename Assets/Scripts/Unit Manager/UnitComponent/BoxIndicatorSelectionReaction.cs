using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoxIndicatorSelectionReaction : MonoBehaviour, ISelectionReactable
{
    [SerializeField] private SpriteRenderer indicatorSprite;
    [SerializeField] private GameObject indicatorParent;

    public void ReactSelect()
    {
        indicatorParent.SetActive(true);
    }

    public void ReactDeselect()
    {
        indicatorParent.SetActive(false);
    }
}
