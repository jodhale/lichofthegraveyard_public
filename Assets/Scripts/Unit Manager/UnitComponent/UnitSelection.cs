using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class UnitSelection : UnitComponents
{
    [SerializeField] private List<ISelectionReactable> selectionReactables;

    private void Awake()
    {
        selectionReactables = new List<ISelectionReactable>();

        ISelectionReactable[] allSelectionComponents = GetComponents<ISelectionReactable>();

        allSelectionComponents.ToList().ForEach(selectComponent => selectionReactables.Add(selectComponent));

    }

    public void SelectUnit()
    {
        selectionReactables.ForEach(reactors => reactors.ReactSelect());
    }

    public void DeselectUnit()
    {
        selectionReactables.ForEach(reactors => reactors.ReactDeselect());
    }
}

public interface ISelectionReactable
{
    void ReactSelect();
    void ReactDeselect();
}