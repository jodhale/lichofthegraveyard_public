using UnityEngine;

[CreateAssetMenu(menuName ="Variable/GameObject")]
public class SOGameObject : SOVariable<GameObject>{ }
