﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SOList<T> : ScriptableObject
{
    public List<T> list;

}
