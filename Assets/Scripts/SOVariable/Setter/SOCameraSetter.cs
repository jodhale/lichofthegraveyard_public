using UnityEngine;

public class SOCameraSetter : SOSetter<SOCamera>
{
    [SerializeField] private Camera cameraToSet;

    private void Awake()
    {
        SOVariableToSet.Value = cameraToSet;
    }
}
