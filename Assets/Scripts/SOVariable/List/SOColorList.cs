﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    [CreateAssetMenu(menuName = "Variable/List/Color")]
public class SOColorList : SOList<Color>{ }
