using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(menuName = "Variable/List/Rigidbody")]
public class SORigidbodyList : SOList<Rigidbody> { }
