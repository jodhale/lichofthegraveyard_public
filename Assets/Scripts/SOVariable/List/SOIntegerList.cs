using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Variable/List/Integer")]
public class SOIntegerList : SOList<int> { }
