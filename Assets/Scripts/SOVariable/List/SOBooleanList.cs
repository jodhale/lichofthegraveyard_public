﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    [CreateAssetMenu(menuName = "Variable/List/Boolean")]
public class SOBooleanList : SOList<bool>{ }
