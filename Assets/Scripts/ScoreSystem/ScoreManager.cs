using UnityEngine;
using Sirenix.OdinInspector;
using UnityAtoms.BaseAtoms;

public class ScoreManager : Singleton<ScoreManager>
{
    [SerializeField] private IntVariable highestScore;
    [SerializeField] private IntVariable currentScore;
    [SerializeField] private VoidEvent onScoreUpdate;
    [SerializeField] private VoidEvent onHighscoreUpdate;

    private const string scoreID = "highscore";

    private void Awake()
    {
        highestScore.Value = PlayerPrefs.GetInt(scoreID);
        onHighscoreUpdate.Raise();
        SetScore(0);
    }

    public void AddScore()
    {
        currentScore.Value++;
        onScoreUpdate.Raise();
    }

    public void SetScore(int value)
    {
        currentScore.Value = value;
        onScoreUpdate.Raise();
    }

    [Button]
    public void ClearSaveData()
    {
        PlayerPrefs.DeleteKey(scoreID);
    }

    public void SaveToHighscore()
    {
        if(currentScore.Value > highestScore.Value)
        {
            highestScore.Value = currentScore.Value;
            PlayerPrefs.SetInt(scoreID, highestScore.Value);
        }

        onHighscoreUpdate.Raise();
    }
}
