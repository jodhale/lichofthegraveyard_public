using UnityEngine;
using UnityEngine.Events;

public class Damager : MonoBehaviour
{
    [SerializeField] private string damageType;
    [SerializeField] private int damageValue;
    [SerializeField] UnityEvent OnDamageDealt;

    Unit unitToDamage;

    public void Initialize(Unit targetUnit)
    {
        unitToDamage = targetUnit;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (unitToDamage is null) return;

        if (other.gameObject.TryGetComponent<IDamageable>(out IDamageable damageable))
        {
            if (other.gameObject == unitToDamage.gameObject)
            {
                damageable.Damage(damageValue);
                OnDamageDealt.Invoke();
            }
        }
    }
}


public interface IDamageable
{
    void Damage(int value);
}