using UnityEngine;

public class UnitFactory : Singleton<UnitFactory>
{
    [SerializeField] private SOTransform gameplayParent;

    public Unit CreateUnit(GameObject objectPrefab)
    {
        Unit newUnitSpawned = Instantiate(objectPrefab).GetComponent<Unit>();
        newUnitSpawned.transform.SetParent(gameplayParent.Value);
        return newUnitSpawned;
    }
}
