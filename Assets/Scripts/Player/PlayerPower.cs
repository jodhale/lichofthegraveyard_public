using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityAtoms.BaseAtoms;
using TMPro;
using UnityEngine.Events;

public class PlayerPower : PlayerComponent
{
	[SerializeField, Range (-1, 1)] float angleLimit = 0.7f;
	[SerializeField] Transform rotateTransform;
	[SerializeField] IntVariable soulCount;
	[SerializeField] IntVariable maxSoulCount;
	[SerializeField] FloatConstant maxDistance;
	[SerializeField] SoulList souls;
	[SerializeField] Image unitImage;
	[SerializeField] TextMeshProUGUI unitTextGUi;
	[SerializeField] UnityEvent OnAbsorb;
	[SerializeField] UnityEvent OnBind;

	SoulActor curSoul;
	
	void Awake()
	{
		souls.list.Clear();
		maxSoulCount.Reset();
		soulCount.Reset();
	}

	void Update()
	{
		Vector3 forward = rotateTransform.TransformDirection(Vector3.forward);

		curSoul = null;
		float closestDistance = 0;

		foreach(SoulActor soul in souls.list)
		{
			if(Vector3.Distance(transform.position, soul.gameObject.transform.position) > maxDistance.Value)
			{
				continue;
			}

			Vector3 targetPos = soul.gameObject.transform.position - transform.position;

			if (Vector3.Dot(forward, targetPos) > angleLimit)
			{
				float distance = Vector2.Distance(targetPos, transform.position);

				if(curSoul == null)
				{
					curSoul = soul;
					closestDistance = distance;
				}

				if(closestDistance > distance)
				{
					curSoul = soul;
					closestDistance = distance;
				}
			}
		}

		bool hasSoul = curSoul != null;

		unitImage.sprite = hasSoul ? curSoul.GetUnitImage() : null;
		unitImage.color = hasSoul ? Color.white : Color.clear;
		unitTextGUi.text = hasSoul ? curSoul.GetUnitName() : "";
	}

	public void Absorb()
	{
		if (curSoul == null)
		{
			return;
		}

		curSoul.Absorb();
		OnBind.Invoke();
	}

	public void Bind()
	{
		if (curSoul == null)
		{
			return;
		}

		curSoul.Bind();
		OnBind.Invoke();
	}
}
