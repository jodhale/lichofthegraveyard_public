using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerUnitManagerController : PlayerController<SOUnitManager>
{

    private bool isHoldingDownSelect;

    public override void AwakeMethod()
    {
        base.AwakeMethod();

        playerInputActions.UnitManagement.UnitSpawn.performed += ExecuteSpawn;
        playerInputActions.UnitManagement.Select.performed += ExecuteStartSelect;
        playerInputActions.UnitManagement.Select.canceled += ExecuteApplySelection;
        playerInputActions.UnitManagement.SetInclusiveSelection.performed += ExecuteInclusive;
        playerInputActions.UnitManagement.SetInclusiveSelection.canceled += ExecuteExclusive;
        playerInputActions.UnitManagement.CommandUnits.performed += ExecuteCommand;

        //selection
        playerInputActions.UnitSpawning.WarriorSelect.performed += ExecuteSpawnWarrior;
        playerInputActions.UnitSpawning.RangeSelect.performed += ExecuteSpawnRange;
        playerInputActions.UnitSpawning.ChonkySelect.performed += ExecuteSpawnChonky;
    }

    public override void ControlUpdate()
    {
        if (isHoldingDownSelect)
        {
            controlledObject.Value.CheckAndSetIfDragSelecting();
        }
    }

    private void ExecuteSpawn(InputAction.CallbackContext obj)
    {
        controlledObject.Value.SpawnOnPoint();
    }

    private void ExecuteStartSelect(InputAction.CallbackContext obj)
    {
        controlledObject.Value.StartSelect();
        isHoldingDownSelect = true;
    }

    private void ExecuteApplySelection(InputAction.CallbackContext obj)
    {
        controlledObject.Value.ApplySelection();
        isHoldingDownSelect = false;
    }

    private void ExecuteInclusive(InputAction.CallbackContext obj)
    {
        controlledObject.Value.SetSelectionInclusive(true);
    }

    private void ExecuteExclusive(InputAction.CallbackContext obj)
    {
        controlledObject.Value.SetSelectionInclusive(false);
    }
    private void ExecuteCommand(InputAction.CallbackContext obj)
    {
        controlledObject.Value.CommandUnits();
    }

    private void ExecuteSpawnWarrior(InputAction.CallbackContext obj)
    {
        controlledObject.Value.SelectRosterByIndex(0);
    }

    private void ExecuteSpawnRange(InputAction.CallbackContext obj)
    {

        controlledObject.Value.SelectRosterByIndex(1);
    }
    private void ExecuteSpawnChonky(InputAction.CallbackContext obj)
    {

        controlledObject.Value.SelectRosterByIndex(2);
    }
}