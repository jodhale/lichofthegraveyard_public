using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityAtoms.BaseAtoms;
using UnityEngine.Events;

public class PlayerAttack : MonoBehaviour
{
	[SerializeField] Transform rotateTransform;
	[SerializeField] GameObject projectilePrefab;
	[SerializeField] IntConstant projectileCount;
	[SerializeField] FloatConstant attackInterval;
	[SerializeField] SOTransform gameplayParent;
	[SerializeField] UnityEvent OnAttack;

	readonly List<GameObject> projectiles = new();
	int projectileIndex;
	WaitForSeconds delay;
	bool canAttack;

	void Start()
	{
		for(int i = 0; i < projectileCount.Value; i++)
		{
			projectiles.Add(Instantiate(projectilePrefab, gameplayParent.Value));
		}

		delay = new WaitForSeconds(attackInterval.Value);
		StartCoroutine(Fire());
	}

	public void SetCanAttack(bool canAttack) => this.canAttack = canAttack;

	IEnumerator Fire()
	{
		if (canAttack)
		{
			GameObject projectile = projectiles[++projectileIndex % projectiles.Count];

			projectile.transform.position = transform.position;
			projectile.transform.eulerAngles = rotateTransform.eulerAngles;
			projectile.SetActive(true);
			OnAttack.Invoke();
			yield return delay;
		}
		else
		{
			yield return null;
		}
		
		StartCoroutine(Fire());
	}
}
