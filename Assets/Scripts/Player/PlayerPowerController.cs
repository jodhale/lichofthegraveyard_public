using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerPowerController : PlayerController<PlayerPowerController>
{
	[SerializeField] PlayerPower power;
	[SerializeField] PlayerAttack attack;

	void Start()
	{
		playerInputActions.PlayerPower.Absorb.performed += Absorb_performed;
		playerInputActions.PlayerPower.Bind.performed += Bind_performed;
		playerInputActions.PlayerPower.Attack.performed += Attack_performed;
		playerInputActions.PlayerPower.Attack.canceled += Attack_canceled;
	}

	private void Attack_canceled(InputAction.CallbackContext obj)
	{
		attack.SetCanAttack(false);
	}

	private void Attack_performed(InputAction.CallbackContext obj)
	{
		attack.SetCanAttack(true);
	}

	private void Absorb_performed(InputAction.CallbackContext obj)
	{
		power.Absorb();
	}

	private void Bind_performed(InputAction.CallbackContext obj)
	{
		power.Bind();
	}
}
