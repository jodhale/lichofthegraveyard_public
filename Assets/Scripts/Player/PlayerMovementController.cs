using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovementController : PlayerController<PlayerMovementController>
{
	[SerializeField] PlayerMovement movement;

	void Update()
	{
		movement.Move(playerInputActions.PlayerMovement.Movement.ReadValue<Vector2>());
		movement.Rotate(playerInputActions.PlayerMovement.Mouse.ReadValue<Vector2>());
	}
}
