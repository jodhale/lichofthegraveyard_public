using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : PlayerComponent
{
	[SerializeField] float speed;
	[SerializeField] float height;
	[SerializeField] Transform rotateTransform;
	[SerializeField] Vector3 centerOffset;
	[SerializeField] Rect border;

	public void Move(Vector2 dir)
	{
		Vector3 pos = transform.position + new Vector3(-dir.x, 0, -dir.y) * Time.deltaTime * speed;
		pos = new Vector3(Mathf.Clamp(pos.x, border.x, border.width), height, Mathf.Clamp(pos.z, border.y, border.height));
		transform.position = pos;
	}

	public void Rotate(Vector2 dir)
	{
		Vector2 playerScreenPosition = Camera.main.WorldToScreenPoint(transform.position + centerOffset);
		rotateTransform.eulerAngles = new Vector3(0, GetDegree(playerScreenPosition, dir) + 180, 0);
	}

	float GetDegree(Vector3 origin, Vector3 target)
	{
		float degree = Mathf.Rad2Deg * Mathf.Atan2(target.x - origin.x, target.y - origin.y);

		if (degree < 0)
		{
			degree += 360;
		}

		return degree;
	}
}
