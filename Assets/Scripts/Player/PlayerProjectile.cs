using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityAtoms.BaseAtoms;
using UnityEngine.Events;

public class PlayerProjectile : MonoBehaviour
{
	[SerializeField] FloatConstant speed;
	[SerializeField] IntConstant damage;
	[SerializeField] UnitFaction targetFaction;
	[SerializeField] UnityEvent OnDamageDealt;

	void Update() => transform.position += transform.forward * speed.Value * Time.deltaTime;

	private void OnTriggerEnter(Collider other)
	{
		if(other.transform.TryGetComponent(out IDamageable damagable))
		{
			if(other.transform.TryGetComponent(out Unit unit))
			{
				if(unit.unitFaction == targetFaction)
				{
					damagable.Damage(damage.Value);
					gameObject.SetActive(false);
					OnDamageDealt.Invoke();
				}
			}
		}
	}
}
