using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enactor : MonoBehaviour
{
   [SerializeField] private IEnactable enactable;
}

public interface IEnactable
{
    void Enact();
}