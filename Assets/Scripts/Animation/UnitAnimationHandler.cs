using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitAnimationHandler : AnimationHandler
{
    public void PlayIdle()
    {
        targetAnimator.Play("Idle");
    }

    public void TriggerHit()
    {
        targetAnimator.Play("Hit");
    }

    public void TriggerAttack()
    {
        targetAnimator.Play("Attack");
    }

    public void TriggerDeath()
    {
        targetAnimator.Play("Death");
    }

    public void SetToRun(bool value)
    {
        targetAnimator.SetBool("isRunning", value);
    }
}
