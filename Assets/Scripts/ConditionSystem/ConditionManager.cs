using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System.Linq;
using UnityAtoms.BaseAtoms;

public class ConditionManager : Singleton<ConditionManager>
{

    [SerializeField] private ConditionStatus currentConditionStatus;
    [SerializeField] private bool isGameWinnable;
    [SerializeField, ShowIf(nameof(isGameWinnable))] private VoidEvent onPlayerWin;
    [SerializeField] private bool isGameLosable;
    [SerializeField, ShowIf(nameof(isGameLosable))] private VoidEvent onPlayerLose;

    private ConditionMessage currentStatusMessage;

    private List<IConditionWinnable> allWinConditions;
    private List<IConditionLosable> allLoseConditions;

    public ConditionStatus CurrentConditionStatus { get => currentConditionStatus; }

    private void Awake()
    {
        if (isGameWinnable) allWinConditions = new List<IConditionWinnable>();
        if (isGameLosable) allLoseConditions = new List<IConditionLosable>();
    }

    public void AddWinCondition(IConditionWinnable winCondition)
    {
        allWinConditions.Add(winCondition);
    }

    public void AddLoseCondition(IConditionLosable loseCondition)
    {

        allLoseConditions.Add(loseCondition);
    }

    public void CheckAllWinCondition(ConditionMessage message)
    {
        if (allWinConditions.All(x => x.IsWinConditionMet()))
        {
            ForceWin(message);
        }
    }

    public void CheckAllLosableCondition(ConditionMessage message)
    {
        if (allLoseConditions.All(x => x.IsLoseConditionMet()))
        {
            ForceLose(message);
        }
    }

    public void ForceWin(ConditionMessage message)
    {
        if (currentConditionStatus != ConditionStatus.None) return;

        onPlayerWin.Raise();
        currentStatusMessage = message;
        currentConditionStatus = ConditionStatus.Win;
    }

    public void ForceLose(ConditionMessage message)
    {
        if (currentConditionStatus != ConditionStatus.None) return;

        currentStatusMessage = message;
        onPlayerLose.Raise();
        currentConditionStatus = ConditionStatus.Lose;
    }

    public void ResetConditionData()
    {
        allWinConditions.Clear();
        allLoseConditions.Clear();
        currentConditionStatus = ConditionStatus.None;
    }

    public void GetMessageAndSetTo(StringVariable stringVariable)
    {
        if (currentConditionStatus != ConditionStatus.None) return;

        stringVariable.Value = currentStatusMessage.ConditionStatusMessage;
    }
}

public class ConditionMessage
{
    private string conditionStatusMessage;

    public string ConditionStatusMessage { get => conditionStatusMessage; }

    public ConditionMessage(string conditionStatusMessage)
    {
        this.conditionStatusMessage = conditionStatusMessage;
    }
}


public interface IConditionLosable
{
    bool IsLoseConditionMet();
}

public interface IConditionWinnable
{
    bool IsWinConditionMet();
}

public enum ConditionStatus
{
    None,
    Win,
    Lose
}