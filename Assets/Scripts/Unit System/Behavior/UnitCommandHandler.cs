using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Script Automaticall handled by the unit manager
public class UnitCommandHandler : UnitComponents
{
    [SerializeField] UnitState MoveToDestinationState;
    [SerializeField] UnitStateManager unitStateManager;
    [SerializeField] private float listeningDuration;
    private CountDownTimer listeningTimer;

    private void Start()
    {
        listeningTimer = new CountDownTimer(listeningDuration, 0f, 1f);
    }

    public void TriggerMove()
    {
        unitStateManager.SwitchState(MoveToDestinationState);
    }
}
