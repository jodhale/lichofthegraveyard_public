using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityAtoms.BaseAtoms;

public class UnitScoreHandler : UnitComponents
{
    [SerializeField] IntVariable score;

    public void AddScore()
    {
        score.Value += 1;
    }
}
