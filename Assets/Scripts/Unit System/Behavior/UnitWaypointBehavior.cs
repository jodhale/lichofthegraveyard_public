using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Sirenix.OdinInspector;

public class UnitWaypointBehavior : UnitComponents
{
    private int currentWaypointIndex;
    [SerializeField, ReadOnly] private Transform currentWaypoint;

    [SerializeField] private NavMeshAgent unitAgent;

    private WaypointSequenceHandler assignedWaypointSequence;

    public bool HasReachedEnd
    {
        get
        {
            if (assignedWaypointSequence is null) return true;

            return currentWaypointIndex >= assignedWaypointSequence.TotalWaypoints;
        }
    }

    public void Initialize(WaypointSequenceHandler waypointSequence)
    {
        assignedWaypointSequence = waypointSequence;
    }

    public void SetNextWaypoint(int waypointIndex)
    {
        currentWaypoint = assignedWaypointSequence.GetNextWaypoint(waypointIndex);

        if (currentWaypoint is null) return;
        unitAgent.SetDestination(currentWaypoint.position);
    }

    public void SetStartingPosition(Vector3 position)
    {
        unitAgent.Warp(position);
        transform.position = position;
    }

    public void SetStartingWaypoint(Transform targetPoint)
    {
        currentWaypointIndex = 1;
        currentWaypoint = targetPoint;
        unitAgent.SetDestination(currentWaypoint.position);
    }

    public void CheckIfPointIsReachAndMovetoNext()
    {
        if (!unitAgent.pathPending)
        {
            if (unitAgent.remainingDistance <= unitAgent.stoppingDistance)
            {
                if (!unitAgent.hasPath || unitAgent.velocity.sqrMagnitude == 0f)
                {
                    SetNextWaypoint(++currentWaypointIndex);
                }
            }
        }
    }
}
