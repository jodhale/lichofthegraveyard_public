using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class UnitMovementBehavior : UnitComponents
{
    [SerializeField] private NavMeshAgent agent;

    private Vector3 lastSavedPosition;

    private void Start()
    {
      //  SetMovementSpeed(entity.baseSpeed.Value);
    }

    public void SetMovementSpeed(float value)
    {
        agent.speed = value;
    }

    public void StopOnCurrentPosition()
    {
        agent.destination = agent.transform.position;
        lastSavedPosition = agent.transform.position;
    }
    public void MoveTowardsPoint(Vector3 targetPosition)
    {
        agent.destination = targetPosition;
        lastSavedPosition = targetPosition;
    }

    public bool HasReachedTargetPoint()
    {
        if (!agent.pathPending)
        {
            if (agent.remainingDistance <= agent.stoppingDistance)
            {
                if (!agent.hasPath || agent.velocity.sqrMagnitude == 0f)
                {
                    return true;
                    // Done
                }
            }
        }

        return false;
    }
}
