using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityAtoms.BaseAtoms;

public class UnitNumberCounter : MonoBehaviour
{
    [SerializeField] private IntVariable currentTotalUnit;

    private void OnEnable()
    {
        currentTotalUnit.Add(1);
    }

    private void OnDisable()
    {
        currentTotalUnit.Subtract(1);
    }
}
