using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitAttackBehavior : UnitComponents
{
    [SerializeField] private UnitAnimationHandler unitAnimation;

    [SerializeField] private float attackDistance;
    [SerializeField] private float attackInterval;
    [SerializeField] private Attack attackSet;

    private CountDownTimer attackSequenceTimer;

    public float AttackDistance { get => attackDistance; }

    private void Start()
    {
        attackSequenceTimer = new CountDownTimer(attackInterval, 0f, 1f);
    }

    public bool IsWithinAttackDistance(Unit targetObject)
    {
        if (Vector3.Distance(transform.position, targetObject.transform.position) <= attackDistance)
        {
            return true;
        }
        else return false;
    }

    public void ResetAttack()
    {
        attackSequenceTimer.Restart();
    }

    public void ExecuteAttackSequence(Unit targetObject)
    {
        if (!attackSequenceTimer.IsFinished())
        {
            attackSequenceTimer.Tick(Time.deltaTime);
        }
        else
        {
            //play animation
            //play audio
            unitAnimation.TriggerAttack();
            attackSet.ExecuteAttack(targetObject);
            print("Attack");
            attackSequenceTimer.Restart();
        }
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, AttackDistance);
    }
#endif

}
