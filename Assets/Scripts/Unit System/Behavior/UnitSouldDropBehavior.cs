using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitSouldDropBehavior : UnitComponents
{
    [SerializeField] SoulActor soul;

    public void DropSoul()
    {
        Instantiate(soul.gameObject, transform.position, Quaternion.identity);
    }
}
