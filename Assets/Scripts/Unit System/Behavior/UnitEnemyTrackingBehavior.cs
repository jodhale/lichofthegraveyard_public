using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitEnemyTrackingBehavior : UnitComponents
{
    [SerializeField] private UnitFaction targetFaction;
    [SerializeField] private Transform detectionPoint;
    [SerializeField] private float trackingRadius;
    [SerializeField] private LayerMask unitLayers;

    private List<Unit> enemyUnits;
    private Unit nearestEnemyUnit;
    private float nearestDistance;

    private void Start()
    {
        enemyUnits = new List<Unit>();
    }

    public void RefereshUnitCheck()
    {
        enemyUnits = new List<Unit>();
    }

    public Unit DetectEnemy()
    {
        enemyUnits.Clear();
        nearestEnemyUnit = null;
        nearestDistance = Mathf.Infinity;

        Collider[] collidedObjects = Physics.OverlapSphere(detectionPoint.position, trackingRadius, unitLayers);

        foreach (Collider acquiredObject in collidedObjects)
        {
            if (acquiredObject.TryGetComponent(out Unit acquiredUnit))
            {
                if (acquiredUnit != entity && acquiredUnit.unitFaction == targetFaction)
                    enemyUnits.Add(acquiredUnit);
            }
        }

        if (enemyUnits.Count <= 0) return null;

        foreach (Unit enemyUnit in enemyUnits)
        {
            float calculatedDistance = Vector3.Distance(detectionPoint.position, enemyUnit.transform.position);

            if (calculatedDistance < nearestDistance)
            {
                nearestEnemyUnit = enemyUnit;
                nearestDistance = calculatedDistance;
            }
        }

        return nearestEnemyUnit;
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(detectionPoint.position, trackingRadius);
    }
#endif

}
