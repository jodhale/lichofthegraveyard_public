using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Sirenix.OdinInspector;

public class UnitStateManager : UnitComponents
{
    [SerializeField] private Transform statesParent;
    [SerializeField] private bool isStateManagerActive = true;

    [SerializeField, ReadOnly] private UnitState currentState;
    private List<UnitState> statesAttached;

    public UnitState CurrentState { get => currentState; }

    private void Start()
    {
        statesAttached = statesParent.GetComponents<UnitState>().ToList();

        UnitState acquiredStartingState = statesAttached.Find(state => state.isStartingState);

        SwitchState(acquiredStartingState);
    }

    public void SwitchState(UnitState targetState)
    {
        currentState = targetState;
        currentState.EnterState(this);
    }

    private void Update()
    {
        if (!isStateManagerActive || currentState is null) return;

        currentState.UpdateState(this);
    }
}
