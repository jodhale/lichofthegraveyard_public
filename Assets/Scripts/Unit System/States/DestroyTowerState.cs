using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyTowerState : UnitState
{
    [SerializeField] SOTowerActor towerActor;
    [SerializeField] UnitMovementBehavior movementBehavior;
    [SerializeField] UnitAttackBehavior attackBehavior;

    private Unit towerUnit;

    public override void EnterState(UnitStateManager unitStateManager)
    {
        towerUnit = towerActor.Value.GetComponent<Unit>();
    }

    public override void UpdateState(UnitStateManager unitStateManager)
    {
        if (towerUnit is null) return;

        if (attackBehavior.IsWithinAttackDistance(towerUnit))
        {
            attackBehavior.ExecuteAttackSequence(towerUnit);
            movementBehavior.StopOnCurrentPosition();
        }
        else
        {
            movementBehavior.MoveTowardsPoint(towerUnit.transform.position);
        }
    }
}
