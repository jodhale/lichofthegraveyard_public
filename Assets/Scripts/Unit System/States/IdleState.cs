using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleState : UnitState
{
    [SerializeField] private UnitEnemyTrackingBehavior trackingBehavior;
    [SerializeField] private UnitAnimationHandler unitAnimation;
    [SerializeField] UnitState AttackState;

    public override void EnterState(UnitStateManager unitStateManager)
    {
        unitAnimation.SetToRun(false);
        unitAnimation.PlayIdle();
        trackingBehavior.RefereshUnitCheck();
        print(unitStateManager.gameObject.name + " has entered idle state");
    }

    public override void UpdateState(UnitStateManager unitStateManager)
    {
        if (trackingBehavior.DetectEnemy() != null)
        {
            unitStateManager.SwitchState(AttackState);
        }
    }
}
