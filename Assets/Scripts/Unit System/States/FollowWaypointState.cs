using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowWaypointState : UnitState
{
    [SerializeField] private UnitWaypointBehavior waypointBehavior;
    [SerializeField] private UnitEnemyTrackingBehavior enemyTrackingBehavior;

    [SerializeField] private UnitState destroyTowerState;
    [SerializeField] private UnitState attackState;

    public override void EnterState(UnitStateManager unitStateManager)
    {
        print("Entered follow Waypoint State");
    }

    public override void UpdateState(UnitStateManager unitStateManager)
    {
        if (!waypointBehavior.HasReachedEnd)
            waypointBehavior.CheckIfPointIsReachAndMovetoNext();
        else
            unitStateManager.SwitchState(destroyTowerState);

        if (enemyTrackingBehavior.DetectEnemy() != null)
        {
            unitStateManager.SwitchState(attackState);
        }
    }
}
