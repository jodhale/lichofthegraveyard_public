using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class UnitState : MonoBehaviour
{
    public bool isStartingState;

    public abstract void EnterState(UnitStateManager unitStateManager);

    public abstract void UpdateState(UnitStateManager unitStateManager);
}
