using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToDestinationCommandState : UnitState
{
    [SerializeField] private UnitAnimationHandler unitAnimation;
    [SerializeField] private UnitMovementBehavior movementBehavior;
    [SerializeField] private UnitState idleState;

    public override void EnterState(UnitStateManager unitStateManager)
    {
        unitAnimation.SetToRun(true);
        print("entered move command state");
    }

    public override void UpdateState(UnitStateManager unitStateManager)
    {

        if (movementBehavior.HasReachedTargetPoint())
        {
            unitStateManager.SwitchState(idleState);
        }
    }
}
