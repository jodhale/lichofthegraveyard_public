using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackState : UnitState
{
    [SerializeField] UnitAnimationHandler unitAnimation;
    [SerializeField] private UnitMovementBehavior movementBehavior;
    [SerializeField] private UnitAttackBehavior attackBehavior;
    [SerializeField] private UnitEnemyTrackingBehavior trackingBehavior;

    [SerializeField] private UnitState NonAttackingState;

    public override void EnterState(UnitStateManager unitStateManager)
    {

    }

    public override void UpdateState(UnitStateManager unitStateManager)
    {
        Unit nearestEnemy = trackingBehavior.DetectEnemy();

        if (nearestEnemy != null)
        {
            if (attackBehavior.IsWithinAttackDistance(nearestEnemy))
            {
                attackBehavior.ExecuteAttackSequence(nearestEnemy);
                movementBehavior.StopOnCurrentPosition();
                unitAnimation.SetToRun(false);
            }
            else
            {
                unitAnimation.SetToRun(true);
                movementBehavior.MoveTowardsPoint(nearestEnemy.transform.position);
            }
        }
        else
        {
            unitStateManager.SwitchState(NonAttackingState);
        }
    }
}
