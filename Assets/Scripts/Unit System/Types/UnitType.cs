using UnityEngine;

[CreateAssetMenu(menuName = "Units System/Unit Type")]
public class UnitType : ScriptableObject
{
	public string UnitName;
	public Sprite UnitIcon;
	public float SoulCost;
    public Unit AdventurerUnitReference;
}
