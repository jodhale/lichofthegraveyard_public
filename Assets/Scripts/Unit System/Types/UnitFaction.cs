using UnityEngine;

[CreateAssetMenu(menuName ="Units System/Faction")]
public class UnitFaction : ScriptableObject { }
