using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeAttack : Attack
{
    [SerializeField] private Projectile projectilePrefab;
    [SerializeField] private Transform projectileStartingPoint;

    public override void ExecuteAttack(Unit targetUnit)
    {
        Projectile newProjectile = Instantiate(projectilePrefab, projectileStartingPoint.position, Quaternion.identity) as Projectile;
        newProjectile.Initialize(targetUnit.transform);
    }
}
