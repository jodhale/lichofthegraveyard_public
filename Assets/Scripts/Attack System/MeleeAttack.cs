using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MeleeAttack : Attack
{
    [SerializeField] private int damageValue;
    [SerializeField] private Transform attackPoint;
    [SerializeField] private UnityEvent OnDamageDealt;
    [SerializeField] private LayerMask unitLayerMask;

    public override void ExecuteAttack(Unit targetUnit)
    {
        Collider[] acquiredCollider = Physics.OverlapSphere(attackPoint.position, 2f, unitLayerMask);

        foreach (Collider targetCollider in acquiredCollider)
        {
            if (targetCollider.gameObject == targetUnit.gameObject)
            {
                if (targetCollider.TryGetComponent(out IDamageable damageable))
                {
                    damageable.Damage(damageValue);
                }
            }
        }
    }
}
