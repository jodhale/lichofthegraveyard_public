using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityAtoms.BaseAtoms;

public class SoulActor : MonoBehaviour
{
	[SerializeField] FloatConstant soulDuration;
	[SerializeField] IntVariable maxSoulCount;
	[SerializeField] SoulList soulList;
	[SerializeField] IntVariable unitBindCount;
	[SerializeField] UnitType unitType;

	float curDuration;
	bool isBeingDestroyed;

	void Start()
	{
		curDuration = soulDuration.Value;
		soulList.list.Add(this);
	}

	void Update()
	{
		if(curDuration < 0)
		{
			DestroySoul();
		}
		else
		{
			curDuration -= Time.deltaTime;
		}
	}

	public Sprite GetUnitImage() => unitType.UnitIcon;

	public string GetUnitName() => unitType.UnitName;

	public void Absorb()
	{
		DestroySoul();
		maxSoulCount.Value++;
		//Do effect
	}

	public void Bind()
	{
		DestroySoul();
		unitBindCount.Value++;
		//Do effect
	}

	void DestroySoul()
	{
		if (!isBeingDestroyed)
		{
			isBeingDestroyed = true;
			soulList.list.Remove(this);
			Destroy(gameObject);
		}
	}
}
