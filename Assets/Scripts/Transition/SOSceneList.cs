using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scene Data/Scene Data List")]
public class SOSceneList : SOList<SOScene> { }
