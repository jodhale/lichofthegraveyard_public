using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityAtoms.BaseAtoms;
using UnityEngine.SceneManagement;

public class TransitionActor : Singleton<TransitionActor>
{
	[SerializeField] Image fadeScreen;
	[SerializeField] float transitionSpeed = 1;
	[SerializeField] SOScene firstScene;
	[SerializeField] private VoidEvent onLevelLoaded;

	string curSceneString = "";
	string newSceneString = "";
	SOScene curScene;
	SOScene newScene;
	float curLerp;
	bool isLoading;
	bool hasUnloadedCurScene;

	private void Start()
	{
		LoadScene(firstScene);
		fadeScreen.raycastTarget = false;
	}

	void Update()
	{
		if (isLoading)
		{
			curLerp += Time.deltaTime * transitionSpeed * (hasUnloadedCurScene ? -1 : 1);
			curLerp = Mathf.Clamp(curLerp, 0, 1);
		}

		fadeScreen.color = Color.Lerp(Color.clear, Color.black, curLerp);
	}

	public SOScene GetCurrentScene() => curScene;

	public void ReloadLevel() => LoadScene(curScene);

	public void LoadScene(SOScene scene)
	{
		if (!isLoading)
		{
			newScene = scene;
			isLoading = true;
			fadeScreen.raycastTarget = true;
			hasUnloadedCurScene = false;
			StartCoroutine(UnloadPhase());
		}
	}

	IEnumerator UnloadPhase()
	{
		yield return new WaitForSeconds(1 / transitionSpeed);

		newSceneString = newScene != null ? newScene.SceneName : curScene.SceneName;

		if (curSceneString != "")
		{
			AsyncOperation op = SceneManager.UnloadSceneAsync(curSceneString);
			op.completed += (obj) => { StartCoroutine(LoadPhase()); };
		}
		else
		{
			StartCoroutine(LoadPhase());
		}
	}

	IEnumerator LoadPhase()
	{
		if (curScene != null)
		{
			curScene.OnUnload.Invoke();
		}

		curSceneString = newSceneString;
		SceneManager.LoadScene(curSceneString, LoadSceneMode.Additive);
		newScene.OnLoad.Invoke();

		curScene = newScene;
		fadeScreen.raycastTarget = false;
		hasUnloadedCurScene = true;

		yield return new WaitForSeconds(1 / transitionSpeed);

		isLoading = false;
		onLevelLoaded.Raise();
	}
}
