using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

using Sirenix.OdinInspector;

[CreateAssetMenu(menuName ="Scene Data/Scene Data")]
public class SOScene : ScriptableObject
{
#if UNITY_EDITOR
	[SerializeField, OnValueChanged(nameof(OnSceneValueChange))] UnityEditor.SceneAsset scene;
#endif

	[SerializeField] public UnityEvent OnLoad;
	[SerializeField] public UnityEvent OnUnload;

	[field: SerializeField] public string SceneName { get; private set; }

	void OnSceneValueChange()
	{

#if UNITY_EDITOR
		UnityEditor.EditorUtility.SetDirty(this);
		SceneName = scene.name;
#endif
	}
}
